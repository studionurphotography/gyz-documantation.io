# Hello VuePress

Hi! I'm your first Markdown file in **StackEdit**. If you want to learn about StackEdit, you can read me. If you want to play with Markdown, you can edit me. Once you have finished with me, you can create new files by opening the **file explorer** on the left corner of the navigation bar.

# Files

StackEdit stores your files in your browser, which means all your files are automatically saved locally and are accessible **offline!**

``` bash

#!/bin/bash
addition(){
   sum=$(($1+$2))
   return $sum
}
read -p "Bir rakam girin: " int1
read -p "Bir rakam girin: " int2
add $int1 $int2
echo "Sonuc : " $?
```

``` python

import sys
for path in sys.path:
    print(path)
```


> markdown !
::: tip
hooray
:::