### Install > Python Package Installation
---
Let me go through the process step by step:
On a computer connected to the internet, create a folder.

```bash
mkdir packages
cd packages
```
open up a command prompt or shell and execute the following command:
Suppose the package you want is tensorflow

```bash
pip3 download tensorflow
```

Now, on the target computer, copy the packages folder and apply the following command

```bash
cd packages
pip3 install 'tensorflow-xyz.whl' --no-index --find-links '.'
```
::: warning
Note that the tensorflow-xyz.whl must be replaced by the original name of the required package.
:::
