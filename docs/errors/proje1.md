### Errors > Nvidia Docker Cuda Error
---

```
Using default tag: latest latest: Pulling from nvidia/cuda 473ede7ed136: Pull complete c46b5fa4d940: 
Pull complete 93ae3df89c92: Pull complete 6b1eed27cade: Pull complete d31e9163d0a5: Pull complete 8668
af631f88: Pull complete 0d99f8ab6ae2: Pull complete 74440c29d798: Pull complete Digest: 
sha256:a6b5fd418d1cd0bc6d8a60c1c4ba33670508487039b828904f8494ec29e6b450 Status: Downloaded newer image
 for nvidia/cuda:latest docker: Error response from daemon: OCI runtime create failed: container_linux
 .go:348: starting container process caused "exec: \"nvidia-smi\": executable file not found in $PATH"
 : unknown.
```

::: tip
You have two errors which are quite self explicites.

First of all, it seems your login user is not allowed to connect to docker daemon. This is quite a standard issue, you just need to add your user login to docker group, It should solve this issue. You will need to logout/login again for this change to become active.

Second, this is also quite a standard linux issue, your shell has a environment variable called PATH , containing all folders where it will be looking for a command binary, when this command doesn't contain full path to the binary.

For exemple, when you typed curl to download docker-nvidia, your shell find it in /usr/bin/ folder, because this folder is declared into the PATH variable.

Same applies for containers you download and different users on your local system. You can investigate this specific error message and find this issue on github: https://github.com/NVIDIA/nvidia-docker/issues/388

:::


::: danger
xxx
:::