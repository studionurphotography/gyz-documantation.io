module.exports = {
  title: 'Baykar GYZ Documentation',
  dest: 'public',
  base: '/vuepress/',
  themeConfig: {
    sidebar: [
      {
        title: 'Installation',
        collapsable: true,
        children: [
          ['/installation/python-package', 'Pip package Installation'],
          ['/installation/docker', 'Docker Installation'],
        ]
      },
      {
        title: 'Onboarding',
        collapsable: true,
        children: [
          ['/onboarding/intern', 'Intern'],
          ['/onboarding/full-time', 'Full Time'],
        ]
      },
      {
        title: 'Errors',
        collapsable: true,
        children: [
          ['/errors/proje1', 'TB-2'],
          ['/errors/proje2', 'Akıncı'],
        ]
      },
    ]
  },
  plugins: ['fulltext-search'],
} 
