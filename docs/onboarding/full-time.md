### Onboarding > Full Time
---

#### Prepare colleagues for the new employee
Prior to the new employee’s first day, send an announcement to all employees, either in-person or via email, welcoming them to the company. The announcement should tell them the new employee’s role, a bit about their experience, what they’ll be doing at your company and encourage other employees to welcome them.
When employees are aware of a new staff member ahead of time, they can be prepared to assist them on their first day. This will go a long way to making the new employee feel welcomed.

#### Have the new employee’s workstation ready to go
Having a “home base” that is ready for your new arrival is crucial to an employee’s first impression of your company. As a new employee, nothing is worse than not having the tools you need to be successful. Setting up the new employee’s computer, email and phone numbers ahead of time, and providing any necessary office supplies, can help a new colleague feel valued from day one. This gives the new employee the tools to perform at their best.

#### Make sure your new employee has access to any necessary programs
In line with getting their workstation ready, make sure the new employee has access to any programs, software or electronic files they will need before their first day. Skipping this step can stunt the new employee’s training, stall their ability to get to work and, in turn, affect their outlook on your company.

#### Make introductions
Schedule some time for the new employee to meet with key people and departments on their first day. Although they may not remember everyone’s name, this will give them a good overview of where to go to get what they need. These introductions will also help them understand how your company works and how their role plays in the overall picture.

#### Plan a team lunch
Arrange a lunch meeting or after-work gathering for the new employee and their immediate team members within the first week. This will help break the ice and allow the employee to get to know their new colleagues in a relaxed environment. You don’t even need to leave the office for this step – employees can bring their lunch and gather in a conference room. Take the time to learn a little bit about your new employee outside of the interview process. When an employee feels valued by their team on both a personal and professional level, they are more likely to stick around for the long haul.  

#### Allow plenty of time for training
The first week or so with the new employee should focus on training. Even if the employee has performed the same job function elsewhere, there are bound to be differences between companies. Having a training plan in place is a vital part of helping new employees find their feet in an organization. Your training should cover company rules, processes, procedures and expectations. Detailing what is expected for new employees sets a precedent by which they can measure their comfort in their new role.
Assigning a mentor from the employee’s department can also help them acclimate to their job by giving them a person who is ready to answer their questions and walk them through some of their assignments.

#### Don’t forget to follow-up
This step is the most important and often the most overlooked by employers. Commit to your 30, 60 and 90-day check-ins with the new employee. Even if the employee is doing well and you feel like they don’t need an evaluation, meet with them. This is your opportunity to learn more about your company’s onboarding process from the employee perspective. Find out what they liked and didn’t like about your process and make changes as you see fit.
Successful onboarding processes reflect the time and effort put into them. Take the time to be thoughtful and do your homework. The first few weeks are the most influential to a new hire’s outlook on your company - positive or negative - and sets the tone for their relationship with your business in the long-term. 