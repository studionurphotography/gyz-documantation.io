// /docs/.vuepress/searchHooks.js
export default {
  async processSuggestions(suggestions, queryString, queryTerms) {
    if (queryString) {
      // add a suggestion to start a search in an external service
      suggestions.push({
        path: 'https://sourcegraph.com/search?patternType=literal&q=',
        slug: queryString,
        parentPageTitle: 'Sourcegraph',
        title: 'Search code',
        contentStr: 'Search for "' + queryString + '" on Sourcegraph',
        external: true,
      })
    }
    return suggestions
  },
 
  async onGoToSuggestion(index, suggestion, queryString, queryTerms) {
    // e.g. create an analytics event
 
    // return true if you want to prevent default navigation
    return true
  },
}
 
// /docs/.vuepress/config.js
// Important: Because of the way Vuepress build works, you cannot use regular import/require,
// code must be provided as plaintext. Hence syntax below is required with fs.readFileSync
const fs = require('fs')
const { path } = require('@vuepress/shared-utils')
 
module.exports = {
  plugins: [
    [
      'fulltext-search',
      {
        // provide the contents of a JavaScript file
        hooks: fs.readFileSync(path.resolve(__dirname, './searchHooks.js')),
      },
    ],
  ],
}